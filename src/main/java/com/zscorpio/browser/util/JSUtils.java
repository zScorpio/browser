package com.zscorpio.browser.util;

/*
    Author: zScorpio
    Time: 19:21 (UTC+2)
    Date: Tuesday - 3/31/2020
*/

import org.cef.browser.CefBrowser;

public class JSUtils {

    public static void execute(CefBrowser cefBrowser, String javascript) {
        cefBrowser.executeJavaScript(javascript, "", 0);
    }

    public static void executeCefQuery(CefBrowser cefBrowser, String query) {
        cefBrowser.executeJavaScript("window.cefQuery({request: " + query + "});", "", 0);
    }
}

package com.zscorpio.browser;

/*
    Author: zScorpio
    Time: 07:21 (UTC+2)
    Date: Tuesday - 3/31/2020
*/

import com.zscorpio.browser.handler.query.DOMChangeHandler;
import com.zscorpio.browser.handler.query.QueryHandler;
import java.io.File;
import org.jsoup.nodes.Document;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class Browser {

    private final BrowserFrame browserFrame;
    private final List<QueryHandler> queryHandlers = new ArrayList<>();

    private Document dom;

    public Browser(String title, String url) {
        browserFrame = new BrowserFrame(this, title, url);
    }

    public void start(boolean removeFocusListener) {
        browserFrame.initialise(removeFocusListener);
    }

    public void setDOMChangeHandler(int domPollInterval, DOMChangeHandler domChangeHandler) {
        browserFrame.setDOMChangeHandler(domPollInterval, domChangeHandler);
    }

    public void addQueryHandler(QueryHandler queryHandler) {
        synchronized (queryHandlers) {
            queryHandlers.add(queryHandler);
        }
    }

    public List<QueryHandler> getQueryHandlers() {
        synchronized (queryHandlers) {
            return queryHandlers;
        }
    }

    public void setDOM(Document dom) {
        this.dom = dom;
    }

    public Document getDOM() {
        return dom;
    }

    public JFrame getJFrame() {
        return browserFrame;
    }
}

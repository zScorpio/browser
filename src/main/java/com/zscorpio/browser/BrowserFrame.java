package com.zscorpio.browser;

/*
    Author: zScorpio
    Time: 20:01 (UTC+2)
    Date: Tuesday - 3/31/2020
*/

import com.zscorpio.browser.handler.MessageRouterHandler;
import com.zscorpio.browser.handler.PageLoadHandler;
import com.zscorpio.browser.handler.query.DOMChangeHandler;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import javax.swing.JFrame;
import org.cef.CefApp;
import org.cef.CefClient;
import org.cef.CefSettings;
import org.cef.browser.CefBrowser;
import org.cef.browser.CefMessageRouter;
import org.cef.handler.CefAppHandlerAdapter;

class BrowserFrame extends JFrame {

    private static final CefApp cefApp;

    private final Browser browser;
    private final String url;
    private int domPollInterval;
    private DOMChangeHandler domChangeHandler;

    static {
        CefApp.addAppHandler(new CefAppHandlerAdapter(null) {
            @Override
            public void stateHasChanged(CefApp.CefAppState state) {
                if (state == CefApp.CefAppState.TERMINATED) {
                    System.exit(0);
                }
            }
        });

        CefSettings settings = new CefSettings();
        settings.windowless_rendering_enabled = false;
        settings.log_severity = CefSettings.LogSeverity.LOGSEVERITY_DISABLE;
        settings.persist_session_cookies = true;
        settings.cache_path = System.getProperty("user.home").concat(File.separator).concat("Documents").concat(File.separator).concat("JCEFBrowser");

        cefApp = CefApp.getInstance(settings);
    }

    public BrowserFrame(Browser browser, String title, String url) {
        super(title);
        this.browser = browser;
        this.url = url;
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                CefApp.getInstance().dispose();
                dispose();
            }
        });

        this.setMinimumSize(new Dimension(1000, 600));
        this.pack();
        this.setLocationRelativeTo(null);
    }

    void initialise(boolean removeFocusListener) {
        Component browserComponent = createBrowserComponent(url);
        if (removeFocusListener) {
            browserComponent.removeFocusListener(browserComponent.getFocusListeners()[0]);
        }
        this.getContentPane().add(browserComponent, BorderLayout.CENTER);
        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    public void setDOMChangeHandler(int domPollInterval, DOMChangeHandler domChangeHandler) {
        this.domPollInterval = domPollInterval;
        this.domChangeHandler = domChangeHandler;
    }

    private Component createBrowserComponent(String url) {
        CefClient client = cefApp.createClient();
        CefMessageRouter cefMessageRouter = CefMessageRouter.create();
        cefMessageRouter.addHandler(new MessageRouterHandler(browser.getQueryHandlers()), true);
        client.addMessageRouter(cefMessageRouter);

        if (domChangeHandler != null) {
            if (domPollInterval <= 0) {
                domPollInterval = 1;
            }
            browser.addQueryHandler(domChangeHandler);
            client.addLoadHandler(new PageLoadHandler(domPollInterval));
        }

        CefBrowser browser = client.createBrowser(url, false, false);
        return browser.getUIComponent();
    }
}

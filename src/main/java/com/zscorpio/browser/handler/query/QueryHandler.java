package com.zscorpio.browser.handler.query;

/*
    Author: zScorpio
    Time: 10:44 (UTC+2)
    Date: Tuesday - 3/31/2020
*/

import org.cef.browser.CefBrowser;

public abstract class QueryHandler {

    private final boolean cache;
    protected String previousRequest = "";

    public QueryHandler(boolean cache) {
        this.cache = cache;
    }

    public boolean validateQuery(CefBrowser cefBrowser, String request) {
        if (validate(cefBrowser, request)) {
            perform(cefBrowser, request);
            if (cache) {
                previousRequest = request;
            }
            return true;
        }
        return false;
    }

    protected abstract boolean validate(CefBrowser cefBrowser, String request);

    protected abstract void perform(CefBrowser cefBrowser, String request);
}

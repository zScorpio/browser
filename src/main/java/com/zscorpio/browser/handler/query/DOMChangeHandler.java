package com.zscorpio.browser.handler.query;

/*
    Author: zScorpio
    Time: 19:39 (UTC+2)
    Date: Tuesday - 3/31/2020
*/

import com.zscorpio.browser.Browser;
import org.cef.browser.CefBrowser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public abstract class DOMChangeHandler extends QueryHandler {

    private final Browser browser;

    public DOMChangeHandler(Browser browser) {
        super(true);
        this.browser = browser;
    }

    @Override
    protected boolean validate(CefBrowser cefBrowser, String request) {
        return request.startsWith("<html") && !request.equals(previousRequest);
    }

    @Override
    protected void perform(CefBrowser cefBrowser, String request) {
        Document dom = Jsoup.parse(request);
        browser.setDOM(dom);
        perform(cefBrowser, dom);
    }

    protected abstract void perform(CefBrowser cefBrowser, Document dom);
}

package com.zscorpio.browser.handler;

/*
    Author: zScorpio
    Time: 19:17 (UTC+2)
    Date: Tuesday - 3/31/2020
*/

import com.zscorpio.browser.util.JSUtils;
import org.cef.browser.CefBrowser;
import org.cef.browser.CefFrame;
import org.cef.handler.CefLoadHandlerAdapter;

public class PageLoadHandler extends CefLoadHandlerAdapter {

    private final int domQueryInterval;
    private boolean pageLoaded = true;
    private boolean startedDOMThread = false;

    public PageLoadHandler(int domQueryInterval) {
        this.domQueryInterval = domQueryInterval;
    }

    @Override
    public void onLoadEnd(CefBrowser browser, CefFrame frame, int httpStatusCode) {
        if (pageLoaded && !startedDOMThread) {
            startedDOMThread = true;
            Thread pollDOM = new Thread(() -> {
                while (true) {
                    JSUtils.executeCefQuery(browser, "document.documentElement.outerHTML");
                    try {
                        Thread.sleep(domQueryInterval);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
            pollDOM.start();
        }
    }

    @Override
    public void onLoadError(CefBrowser browser, CefFrame frame, ErrorCode errorCode, String errorText, String failedUrl) {
        pageLoaded = false;
        browser.reload();
    }
}

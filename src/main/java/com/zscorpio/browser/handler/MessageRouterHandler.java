package com.zscorpio.browser.handler;

/*
    Author: zScorpio
    Time: 07:43 (UTC+2)
    Date: Tuesday - 3/31/2020
*/

import com.zscorpio.browser.handler.query.QueryHandler;
import org.cef.browser.CefBrowser;
import org.cef.browser.CefFrame;
import org.cef.callback.CefQueryCallback;
import org.cef.handler.CefMessageRouterHandlerAdapter;

import java.util.List;

public class MessageRouterHandler extends CefMessageRouterHandlerAdapter {

    private final List<QueryHandler> queryHandlers;

    public MessageRouterHandler(List<QueryHandler> queryHandlers) {
        this.queryHandlers = queryHandlers;
    }

    @Override
    public boolean onQuery(CefBrowser cefBrowser, CefFrame cefFrame, long queryId, String request, boolean persistent, CefQueryCallback cefQueryCallback) {
        for (QueryHandler queryHandler : queryHandlers) {
            if (queryHandler.validateQuery(cefBrowser, request)) {
                break;
            }
        }
        return true;
    }
}